﻿using AutoMapper;
using Ioasys.Desafio.Application.AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Ioasys.Desafio.API.Configuration
{
    public static class AutoMapperSetup
    {
        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper(typeof(DomainToViewModelMappingProfile));
        }
    }
}
