﻿using System.Threading.Tasks;

namespace Ioasys.Desafio.Domain.Core
{
    public interface IUnitOfWork
    {
        Task<bool> Commit();
    }
}
