﻿namespace Ioasys.Desafio.Domain.Core
{
    public interface IRepository
    {
        public IUnitOfWork UnitOfWork { get; }
    }
}
