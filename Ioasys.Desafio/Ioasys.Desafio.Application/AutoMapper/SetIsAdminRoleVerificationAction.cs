﻿using AutoMapper;
using Ioasys.Desafio.Application.ViewModels;
using Ioasys.Desafio.Domain.Identidade;
using Microsoft.AspNetCore.Identity;

namespace Ioasys.Desafio.Application.AutoMapper
{
    public class SetIsAdminRoleVerificationAction : IMappingAction<User, UserViewModel>
    {
        private readonly UserManager<User> _userManager;

        public SetIsAdminRoleVerificationAction(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public void Process(User source, UserViewModel destination, ResolutionContext context)
        {
            destination.IsAdmin = _userManager.IsInRoleAsync(source, "Admin").Result;
        }
    }
}
