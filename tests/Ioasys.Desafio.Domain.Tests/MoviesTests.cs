﻿using Ioasys.Desafio.Domain.Core;
using Ioasys.Desafio.Domain.Movies;
using System;
using Xunit;

namespace Ioasys.Desafio.Domain.Tests
{
    public class MoviesTests
    {
        [Fact]
        public void Movie_AverageRatings_ShouldBeEqualToFour()
        {
            // Arrange
            var movie = new Movie("Title name", "Description");
            var ratings = new Rating(Guid.NewGuid(), 4, movie);

            // Act
            movie.AddRating(ratings);

            // Assert
            Assert.Equal(4, movie.AverageRating);
        }

        [Fact]
        public void Movie_AverageRatings_ShouldThrowsErrorIfIsGreaterThanFour()
        {
            // Arrange
            var movie = new Movie("Title name", "Description");
            var ratings = new Rating(Guid.NewGuid(), 5, movie);

            // Act and Assert
            Assert.Throws<DomainException>(() => movie.AddRating(ratings));
        }
    }
}
