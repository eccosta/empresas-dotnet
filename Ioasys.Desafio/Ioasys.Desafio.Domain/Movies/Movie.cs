﻿using Ioasys.Desafio.Domain.Core;
using System.Collections.Generic;
using System.Linq;

namespace Ioasys.Desafio.Domain.Movies
{
    public class Movie : Entity
    {
        const int MaxRating = 4;

        public Movie(string title, string description)
        {
            Title = title;
            Description = description;
            Directors ??= new List<Director>();
            Writers ??= new List<Writer>();
            Genres ??= new List<Genre>();
            Casters ??= new List<Caster>();
            Ratings ??= new List<Rating>();
        }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public List<Director> Directors { get; private set; }

        public List<Writer> Writers { get; private set; }

        public List<Genre> Genres { get; private set; }

        public List<Caster> Casters { get; private set; }

        public List<Rating> Ratings { get; private set; }

        public void AddRating(Rating rating)
        {
            if (rating.UserRating > MaxRating)
                throw new DomainException($"Ratings above {MaxRating} is not permitted");

            Ratings.Add(rating);
        }

        public int AverageRating
        {
            get
            {
                if (Ratings.Count == 0) return 0;

                return Ratings.Sum(x => x.UserRating) / Ratings.Count();
            }
        }
    }
}
