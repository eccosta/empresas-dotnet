﻿using Ioasys.Desafio.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ioasys.Desafio.Application.Interfaces
{
    public interface IMovieService 
    {
        Task AddMovieAsync(MovieViewModel movie);

        Task RateMovieAsync(Guid movieId, Guid userId, int rating);

        Task<PagedResults<MovieViewModel>> GetMoviesByParamsAsync(int pageSize, int pageIndex, string query);

        Task<IEnumerable<MovieViewModel>> GetMoviesAsync();
    }
}
