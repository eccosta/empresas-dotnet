## Description

A REST Web Api application using Clean Architecture. You can registry users, movies and rating movies.

The Default Admin User is: 

- Username: user@example.com
 
- Password: Teste@123

### Initially, the first run will take some seconds because migrations will run automatically.

## Features
 * Asp NET Identity for Account Control
 * AutoMapper
 * Entity Framework Core for ORM
 * SQL Server
 * Everything Running in Docker.

To use this project, you first need Docker installed. Downloads:

https://www.docker.com/products/docker-desktop

Once Docker is installed you should be able to run the following from command line (terminal / cmd / powershell): docker -v.

## Docker-Compose
Do you have "docker-compose" installed? If you don't have, just check https://docs.docker.com/compose/install/ to install.

## Setting up the application
You can simply run the project using the VSCode or the VStudio if you want. Open a terminal/cmd in the project folder and type the following command:

> docker-compose up

This command will generate the following containers:

> movies-api

> sample-db

And you can access through the URL: http://localhost:5001/swagger
