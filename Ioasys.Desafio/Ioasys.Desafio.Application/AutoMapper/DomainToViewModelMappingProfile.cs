﻿using AutoMapper;
using Ioasys.Desafio.Application.ViewModels;
using Ioasys.Desafio.Domain.Identidade;
using Ioasys.Desafio.Domain.Movies;

namespace Ioasys.Desafio.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<User, UserViewModel>()
                .AfterMap<SetIsAdminRoleVerificationAction>()
                .ReverseMap();

            CreateMap<Movie, MovieViewModel>()
                .ForMember(src => src.Rating, dest => dest.MapFrom(x => x.AverageRating))
                .ReverseMap();

            CreateMap<Director, DirectorViewModel>().ReverseMap();
            CreateMap<Caster, CasterViewModel>().ReverseMap();
            CreateMap<Genre, GenreViewModel>().ReverseMap();
            CreateMap<Writer, WriterViewModel>().ReverseMap();
        }
    }
}
