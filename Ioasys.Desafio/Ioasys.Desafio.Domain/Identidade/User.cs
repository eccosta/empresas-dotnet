﻿using Microsoft.AspNetCore.Identity;

namespace Ioasys.Desafio.Domain.Identidade
{
    public class User : IdentityUser
    {
        public bool IsActive { get; set; }
    }
}
