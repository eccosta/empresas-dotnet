﻿using Ioasys.Desafio.Domain.Core;
using System;

namespace Ioasys.Desafio.Domain.Movies
{
    public class Genre : Entity 
    {
        public string Name { get; private set; }

        public Guid MovieId { get; private set; }
        public Movie Movie { get; private set; }

        public Genre(string name, Movie movie)
        {
            Name = name;
            Movie = movie;
            MovieId = movie.Id;
        }

        //EF
        protected Genre()
        {

        }
    }
}
