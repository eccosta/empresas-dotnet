﻿using System.Collections.Generic;

namespace Ioasys.Desafio.Application.Notifications
{
    public interface INotificator
    {
        IEnumerable<Notification> GetMessages();

        void Handle(Notification message);

        bool HasNotification();
    }
}
