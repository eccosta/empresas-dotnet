using Ioasys.Desafio.API.IntegrationTests.Config;
using Ioasys.Desafio.Application.ViewModels;
using System.Threading.Tasks;
using System.Net.Http;
using Xunit;
using Bogus;

namespace Ioasys.Desafio.API.IntegrationTests
{
    [Collection(nameof(IntegrationApiTestsFixtureCollection))]
    public class IdentityApiTests
    {
        private readonly IntegrationTestsFixture<StartupApiTests> _testsFixture;

        public IdentityApiTests(IntegrationTestsFixture<StartupApiTests> testsFixture)
        {
            _testsFixture = testsFixture;
        }

        [Fact]
        public async Task Identity_NewUser_ShouldReturnSuccess()
        {
            // Arrange
            var faker = new Faker("pt_BR");
            string email = faker.Internet.Email().ToLower();
            string pass = faker.Internet.Password(8, false, "", "Ab1#");

            var content = _testsFixture.GetObjectAsStringContentJson(new UserRegister
            {
                Name = email,
                Email = email,
                Password = pass,
                ConfirmPassword = pass
            });

            //Act
            var postResponse = await _testsFixture.Client.PostAsync("api/accounts/", content);

            //Assert
            postResponse.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Identity_NewUserWithInvalidPassword_ShouldThrownAnError()
        {
            // Arrange
            var content = _testsFixture.GetObjectAsStringContentJson(new UserRegister
            {
                Name = "testB",
                Email = "testB@mail.com",
                Password = "teste",
                ConfirmPassword = "teste"
            });

            //Act
            var postResponse = await _testsFixture.Client.PostAsync("api/accounts/", content);

            //Assert
            Assert.Throws<HttpRequestException>(() => postResponse.EnsureSuccessStatusCode());
        }
    }
}