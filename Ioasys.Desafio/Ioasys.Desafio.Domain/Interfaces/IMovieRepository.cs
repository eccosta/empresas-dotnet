﻿using Ioasys.Desafio.Domain.Core;
using Ioasys.Desafio.Domain.Movies;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ioasys.Desafio.Domain.Interfaces
{
    public interface IMovieRepository : IRepository, IDisposable
    {
        Task<IEnumerable<Movie>> GetMoviesAsync();

        Task<Movie> GetMoviesByIdAsync(Guid movieId);

        Task<IEnumerable<Movie>> GetMoviesByParamsAsync(int pageSize, int pageIndex, string query);

        void AddMovie(Movie movie);

        void AddRating(Rating rating);

        void UpdateMovie(Movie movie);
    }
}
