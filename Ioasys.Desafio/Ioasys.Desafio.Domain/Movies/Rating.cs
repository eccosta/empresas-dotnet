﻿using Ioasys.Desafio.Domain.Core;
using System;

namespace Ioasys.Desafio.Domain.Movies
{
    public class Rating : Entity
    {
        public Guid MovieId { get; private set; }

        public Guid UserId { get; private set; }

        public int UserRating { get; private set; }

        public Movie Movie { get; private set; }

        public Rating(Guid userId, int userRating, Movie movie)
        {
            MovieId = movie.Id;
            UserId = userId;
            UserRating = userRating;
            Movie = movie;
        }
        protected Rating()
        {

        }
    }
}
