﻿using Ioasys.Desafio.Data.Context;
using Ioasys.Desafio.Domain.Core;
using Ioasys.Desafio.Domain.Interfaces;
using Ioasys.Desafio.Domain.Movies;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.Desafio.Data.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MoviesContext _context;

        public IUnitOfWork UnitOfWork => _context;

        public MovieRepository(MoviesContext context)
        {
            _context = context;
        }
        public async Task<Movie> GetMoviesByIdAsync(Guid movieId)
        {
            return await _context.Movies
            .Include(x => x.Ratings)
            .Include(x => x.Directors)
            .Include(x => x.Genres)
            .Include(x => x.Casters)
            .Include(x => x.Writers)
            .FirstOrDefaultAsync(x => x.Id == movieId);
        }
        public async Task<IEnumerable<Movie>> GetMoviesAsync()
        {
            return await _context.Movies
                .Include(x=> x.Ratings)
                .Include(x => x.Directors)
                .Include(x => x.Genres)
                .Include(x => x.Casters)
                .Include(x => x.Writers)
                .ToListAsync();
        }
        public async Task<IEnumerable<Movie>> GetMoviesByParamsAsync(int pageSize, int pageIndex, string query)
        {
            var movies = await _context.Movies
              .Include(x => x.Ratings)
              .Include(x => x.Directors)
              .Include(x => x.Genres)
              .Include(x => x.Casters)
              .Include(x => x.Writers)
              .ToListAsync();

            if (!string.IsNullOrEmpty(query))
            {
                query = query.ToUpper();

                movies = movies.Where(x => x.Directors.Select(s => s.Name.ToUpper()).Contains(query)
                               || x.Title.ToUpper().Contains(query)
                               || x.Genres.Select(s => s.Name.ToUpper()).Contains(query)
                               || x.Casters.Select(s => s.Name.ToUpper()).Contains(query)).ToList();
            }

            return movies
              .OrderByDescending(x => x.AverageRating)
              .ThenBy(x => x.Title)
              .Skip(pageSize * (pageIndex - 1))
              .Take(pageSize);
        }

        public void AddMovie(Movie movie)
        {
            _context.Movies.Add(movie);
        }

        public void AddRating(Rating rating)
        {
            _context.Ratings.Add(rating);
        }

        public void UpdateMovie(Movie movie)
        {
            _context.Movies.Update(movie);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
