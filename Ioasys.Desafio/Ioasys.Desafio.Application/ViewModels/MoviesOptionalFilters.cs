﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.Desafio.Application.ViewModels
{
    public class MoviesOptionalFilters
    {
        public string director  {get; set;}
        public string title { get; set; }
        public string genres { get; set; }
        public string actors { get; set; }
    }
}
