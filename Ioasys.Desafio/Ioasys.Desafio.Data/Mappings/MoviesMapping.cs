﻿using Ioasys.Desafio.Domain.Movies;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.Desafio.Data.Mappings
{
    public class MoviesMapping : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(x => x.Id);            

            builder.Property(x => x.Title).IsRequired();
            builder.Property(builder => builder.Description).IsRequired();

            //Relations
            builder.HasMany(c => c.Directors)
            .WithOne(c => c.Movie)
            .HasForeignKey(c => c.MovieId);

            builder.HasMany(c => c.Casters)
            .WithOne(c => c.Movie)
            .HasForeignKey(c => c.MovieId);

            builder.HasMany(c => c.Ratings)
            .WithOne(c => c.Movie)
            .HasForeignKey(c => c.MovieId);

            builder.HasMany(c => c.Writers)
            .WithOne(c => c.Movie)
            .HasForeignKey(c => c.MovieId);

            builder.HasMany(c => c.Genres)
            .WithOne(c => c.Movie)
            .HasForeignKey(c => c.MovieId);

            builder.ToTable("Movies");
        }
    }
}
