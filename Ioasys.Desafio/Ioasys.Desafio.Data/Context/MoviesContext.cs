﻿using Ioasys.Desafio.Domain.Core;
using Ioasys.Desafio.Domain.Movies;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.Desafio.Data.Context
{
    public class MoviesContext : DbContext, IUnitOfWork
    {
        public MoviesContext(DbContextOptions<MoviesContext> options) : base(options)
        {
            Database.Migrate();
        }
        public DbSet<Movie> Movies{get; set;}
        public DbSet<Director> Directors {get; set;}
        public DbSet<Writer> Writers {get; set;}
        public DbSet<Genre> Genres {get; set;}
        public DbSet<Caster> Casters {get; set;}
        public DbSet<Rating> Ratings { get; set; }

        public async Task<bool> Commit()
        {
            return await SaveChangesAsync() > 0;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var property in modelBuilder.Model.GetEntityTypes().SelectMany(
             e => e.GetProperties().Where(p => p.ClrType == typeof(string))))
                property.SetColumnType("varchar(150)");

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(MoviesContext).Assembly);

            base.OnModelCreating(modelBuilder);
        }
    }
}
