﻿using AutoMapper;
using Ioasys.Desafio.Application.Extensions;
using Ioasys.Desafio.Application.Interfaces;
using Ioasys.Desafio.Application.Notifications;
using Ioasys.Desafio.Application.ViewModels;
using Ioasys.Desafio.Domain.Identidade;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace Ioasys.Desafio.Application.Services
{
    public class AccountService : BaseService, IAccountService
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly TokenSettings _tokenSettings;
        private readonly IMapper _mapper;
        public AccountService(SignInManager<User> signInManager,
                                     UserManager<User> userManager,
                                     INotificator _notificator,
                                     IOptions<TokenSettings> options,
                                     RoleManager<IdentityRole> roleManager,
                                     IMapper mapper) : base(_notificator)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _tokenSettings = options.Value;
            _roleManager = roleManager;
            _mapper = mapper;
        }

        public async Task<UserLoginResponse> RegisterAsync(UserRegister userRegister)
        {
            var user = new User
            {
                UserName = userRegister.Email,
                Email = userRegister.Email,
                EmailConfirmed = true,
                IsActive = true,
            };

            var result = await _userManager.CreateAsync(user, userRegister.Password);

            if (result.Succeeded)
            {
                return await GerarJwt(user.Email);
            }

            foreach (var error in result.Errors)
                AddNotification(error.Description);

            return null;
        }

        public async Task<UserLoginResponse> LoginAsync(UserLogin userLogin)
        {
            var result = await _signInManager.PasswordSignInAsync(userLogin.Email, userLogin.Password, false, true);

            if (result.Succeeded)
                return await GerarJwt(userLogin.Email);

            if (result.IsLockedOut)
            {
                AddNotification("Your account has been blocked due to exceeded login attempts.");
                return null;
            }

            AddNotification("Your username or password is incorrect.");

            return null;
        }

        public async Task<UserViewModel> UpdateUserAsync(UpdateUserViewModel updateUserViewModel)
        {
            var result = await _signInManager.PasswordSignInAsync(updateUserViewModel.Email, updateUserViewModel.CurrentPassword, false, true);

            if (result.Succeeded)
            {
                var user = await _userManager.FindByEmailAsync(updateUserViewModel.Email);
                await ChangePasswordIfIsDifferent(user, updateUserViewModel);
                await ChangeUsernameIfIsDifferent(user, updateUserViewModel.UserName);
                return _mapper.Map<User, UserViewModel>(user);
            }

            if (result.IsLockedOut)
            {
                AddNotification("Your account has been blocked due to exceeded login attempts.");
                return null;
            }

            AddNotification("Your username or password is incorrect.");
            return null;
        }

        async Task ChangePasswordIfIsDifferent(User user, UpdateUserViewModel updateUserViewModel)
        {
            if (updateUserViewModel.CurrentPassword != updateUserViewModel.NewPassword)
            {
                var result = await _userManager.ChangePasswordAsync(user, updateUserViewModel.CurrentPassword, updateUserViewModel.NewPassword);

                if (result.Succeeded) return;

                foreach (var error in result.Errors)
                    AddNotification(error.Description);
            }
        }

        async Task ChangeUsernameIfIsDifferent(User user, string newUserName)
        {
            user.UserName = user.UserName != newUserName ? newUserName : user.UserName;
            await _userManager.UpdateAsync(user);
        }

        public async Task<UserViewModel> SetAdminRoleToUserAsync(Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());

            if (user is null)
            {
                AddNotification("User does not exists!");
                return null;
            }

            var adminRoleExists = await _roleManager.RoleExistsAsync("Admin");

            if (!adminRoleExists)
                await _roleManager.CreateAsync(new IdentityRole("Admin"));

            await _userManager.AddToRoleAsync(user, "Admin");

            return _mapper.Map<User, UserViewModel>(user);
        }

        public async Task<UserViewModel> DisableUserAsync(Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());

            if (user is null)
            {
                AddNotification("User does not exists!");
                return null;
            }

            user.IsActive = false;

            await _userManager.UpdateAsync(user);

            return _mapper.Map<User, UserViewModel>(user);
        }

        public async Task<PagedResults<UserViewModel>> GetActiveNonAdmininUsers(int pageIndex, int pageSize, string query = null)
        {
            var users = _userManager.Users.Where(x => x.IsActive).OrderBy(x => x.UserName).ToList();

            var results = new List<UserViewModel>();

            foreach (var user in users)
            {
                if (!await _userManager.IsInRoleAsync(user, "Admin"))
                    results.Add(_mapper.Map<User, UserViewModel>(user));
            }

            return GetResultsPaginated(results, pageIndex, pageSize, query);
        }

        PagedResults<UserViewModel> GetResultsPaginated(List<UserViewModel> results, int pageIndex, int pageSize, string query = null)
        {
            var resultsPerPageSize = results
                .Where(x => string.IsNullOrEmpty(query) || x.UserName.ToUpper().Contains(query.ToUpper()))
                .Skip(pageSize * (pageIndex - 1))
                .Take(pageSize)
                .ToList();

            return new PagedResults<UserViewModel>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                Query = query,
                Results = resultsPerPageSize,
                TotalResults = results.Count
            };
        }

        private async Task<UserLoginResponse> GerarJwt(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            var claims = await _userManager.GetClaimsAsync(user);
            var identityClaims = await GetUserClaims(claims, user);
            var encodedToken = GenerateToken(identityClaims);

            return GetToken(encodedToken, user, claims);
        }

        private async Task<ClaimsIdentity> GetUserClaims(ICollection<Claim> claims, User user)
        {
            var userRoles = await _userManager.GetRolesAsync(user);

            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.Id));
            claims.Add(new Claim(JwtRegisteredClaimNames.Email, user.Email));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Nbf, ToUnixEpochDate(DateTime.UtcNow).ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(DateTime.UtcNow).ToString(), ClaimValueTypes.Integer64));

            foreach (var userRole in userRoles)
                claims.Add(new Claim("CustomRoles", userRole));

            var identityClaims = new ClaimsIdentity();
            identityClaims.AddClaims(claims);

            return identityClaims;
        }

        string GenerateToken(ClaimsIdentity identityClaims)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_tokenSettings.Secret);
            var token = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Audience = _tokenSettings.Auditor,
                Issuer = _tokenSettings.Issuer,
                Subject = identityClaims,
                Expires = DateTime.UtcNow.AddHours(_tokenSettings.ExpiresIn),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
            });

            var endodedToken = tokenHandler.WriteToken(token);

            return endodedToken;
        }

        private UserLoginResponse GetToken(string encodedToken, IdentityUser user, IEnumerable<Claim> claims)
        {
            return new UserLoginResponse
            {
                AccessToken = encodedToken,
                ExpiresIn = TimeSpan.FromHours(_tokenSettings.ExpiresIn).TotalSeconds,
                UserToken = new UserToken
                {
                    Id = user.Id,
                    Email = user.Email,
                    Claims = claims.Select(c => new UserClaims { Type = c.Type, Value = c.Value })
                }
            };
        }

        private static long ToUnixEpochDate(DateTime date)
            => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
    }
}
