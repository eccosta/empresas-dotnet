﻿using AutoMapper;
using Ioasys.Desafio.Application.Interfaces;
using Ioasys.Desafio.Application.Notifications;
using Ioasys.Desafio.Application.ViewModels;
using Ioasys.Desafio.Domain.Interfaces;
using Ioasys.Desafio.Domain.Movies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.Desafio.Application.Services
{
    public class MovieService : BaseService, IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IMapper _mapper;
        public MovieService(IMovieRepository movieRepository,
                            IMapper mapper,
                            INotificator notificator) : base(notificator)
        {
            _movieRepository = movieRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<MovieViewModel>> GetMoviesAsync()
        {
            var results = await _movieRepository.GetMoviesAsync();

            return _mapper.Map<IEnumerable<Movie>, IEnumerable<MovieViewModel>>(results);
        }

        public async Task<PagedResults<MovieViewModel>> GetMoviesByParamsAsync(int pageSize, int pageIndex, string query)
        {
            var results = await _movieRepository.GetMoviesByParamsAsync(pageSize, pageIndex, query);

            var mappedResults = _mapper.Map<IEnumerable<Movie>, IEnumerable<MovieViewModel>>(results);

            return new PagedResults<MovieViewModel>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                Query = query,
                TotalResults = mappedResults.Count(),
                Results = mappedResults
            };
        }

        public async Task AddMovieAsync(MovieViewModel movie)
        {
            var domainMovie = _mapper.Map<MovieViewModel, Movie>(movie);

            _movieRepository.AddMovie(domainMovie);

            if (!await _movieRepository.UnitOfWork.Commit())
                AddNotification("An error occurs when adding a movie!");
        }

        public async Task RateMovieAsync(Guid movieId, Guid userId, int rating)
        {
            var movie = await _movieRepository.GetMoviesByIdAsync(movieId);

            if (movie is null)
            {
                AddNotification("This movie does not exists!");
                return;
            }

            var userRating = new Rating(userId, rating, movie);
            movie.AddRating(userRating);
            _movieRepository.AddRating(userRating);
            _movieRepository.UpdateMovie(movie);

            if (!await _movieRepository.UnitOfWork.Commit())
                AddNotification("An error occurs when adding a movie!");
        }
    }
}
