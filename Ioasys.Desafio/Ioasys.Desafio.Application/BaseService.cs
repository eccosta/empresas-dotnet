﻿using Ioasys.Desafio.Application.Notifications;

namespace Ioasys.Desafio.Application
{
    public abstract class BaseService
    {
        protected readonly INotificator _notificator;

        protected BaseService(INotificator notificator)
        {
            _notificator = notificator;
        }

        protected void AddNotification(string error)
        {
            _notificator.Handle(new Notification(error));
        }
    }
}
