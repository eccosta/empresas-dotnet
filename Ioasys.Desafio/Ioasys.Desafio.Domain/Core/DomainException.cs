﻿using System;

namespace Ioasys.Desafio.Domain.Core
{
    public class DomainException : Exception
    {
        public DomainException(string message) : base(message)
        {
        }
    }
}
