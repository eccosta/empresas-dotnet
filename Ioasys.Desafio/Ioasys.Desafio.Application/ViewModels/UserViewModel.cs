﻿using System;

namespace Ioasys.Desafio.Application.ViewModels
{
    public class UserViewModel
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }    

        public string Email { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsActive { get; set; }
    }
}
