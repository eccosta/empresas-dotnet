﻿using Ioasys.Desafio.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ioasys.Desafio.Application.Interfaces
{
    public interface IAccountService
    {
        Task<UserLoginResponse> RegisterAsync(UserRegister userRegister);

        Task<UserLoginResponse> LoginAsync(UserLogin userRegister);

        Task<UserViewModel> SetAdminRoleToUserAsync(Guid userId);

        Task<UserViewModel> DisableUserAsync(Guid userId);

        Task<UserViewModel> UpdateUserAsync(UpdateUserViewModel userViewModel);

        Task<PagedResults<UserViewModel>> GetActiveNonAdmininUsers(int pageIndex, int pageSize, string query = null);
    }
}
