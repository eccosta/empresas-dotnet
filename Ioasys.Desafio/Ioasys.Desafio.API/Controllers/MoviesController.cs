﻿using Ioasys.Desafio.API.Extensions;
using Ioasys.Desafio.Application.Extensions;
using Ioasys.Desafio.Application.Interfaces;
using Ioasys.Desafio.Application.Notifications;
using Ioasys.Desafio.Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Ioasys.Desafio.API.Controllers
{
    [Authorize]
    [Route("api/movies")]
    public class MoviesController : BaseController
    {
        private readonly IMovieService _movieService;

        public MoviesController(IMovieService movieService, INotificator notificator) : base(notificator)
        {
            _movieService = movieService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetMovies([FromQuery] int ps = 8, [FromQuery] int page = 1, [FromQuery] string q = null)
        {
            return CustomResponse(await _movieService.GetMoviesByParamsAsync(ps, page, q));
        }

        [HttpPost("")]
        [ClaimsAuthorize("CustomRoles", "Admin")]
        public async Task<IActionResult> CreateMovies(MovieViewModel movieViewModel)
        {
            await _movieService.AddMovieAsync(movieViewModel);

            return CustomResponse(await _movieService.GetMoviesAsync());
        }

        [HttpPatch]
        [Route("rating/{movieId:guid}/{rating:range(0, 4)}")]
        public async Task<IActionResult> RateMovie(Guid movieId, int rating)
        {
            if (HttpContext.User.IsAdminUser())
                return Unauthorized();

            await _movieService.RateMovieAsync(movieId, HttpContext.User.GetUserId(), rating);

            return CustomResponse(await _movieService.GetMoviesAsync());
        }
    }
}
