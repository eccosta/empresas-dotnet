﻿namespace Ioasys.Desafio.Application.ViewModels
{
    public class UserClaims
    {
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
