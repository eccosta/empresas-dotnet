﻿using Ioasys.Desafio.Domain.Identidade;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.Desafio.Data.Context
{
    public class IdentityContext : IdentityDbContext<User>
    {
        public IdentityContext(DbContextOptions<IdentityContext> options) : base(options)
        {
            Database.Migrate();
            SeedDefaultAdminUser();
        }

        void SeedDefaultAdminUser()
        {
            string email = "user@example.com";

            if (this.Users.Any(x => x.Email == email)) return;

            var userId = Guid.NewGuid().ToString();

            var user = new User
            {
                Id = userId,
                Email = email,
                NormalizedEmail = email.ToUpper(),
                UserName = email,
                NormalizedUserName = email.ToUpper(),
                IsActive = true,
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D")
            };

            SetPasswordHash(user).GetAwaiter();
            AssignToAdminRole(userId);
            SaveChangesAsync().GetAwaiter();
        }


        async Task SetPasswordHash(User user)
        {
            var password = new PasswordHasher<User>();
            var hashed = password.HashPassword(user, "Teste@123");
            user.PasswordHash = hashed;

            var userStore = new UserStore<User>(this);
            await userStore.CreateAsync(user);
        }

        void AssignToAdminRole(string userId)
        {
            var roleId = Guid.NewGuid().ToString();

            this.Roles.Add(new IdentityRole
            {
                Id = roleId,
                Name = "Admin",
                NormalizedName = "ADMIN",
                ConcurrencyStamp = Guid.NewGuid().ToString()
            });

            this.UserRoles.Add(new IdentityUserRole<string>
            {
                RoleId = roleId,
                UserId = userId
            });
        }
    }
}
