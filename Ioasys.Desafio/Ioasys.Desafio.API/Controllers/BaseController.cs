﻿using Ioasys.Desafio.Application.Notifications;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Linq;

namespace Ioasys.Desafio.API.Controllers
{
    [ApiController]
    public class BaseController : Controller
    {
        protected INotificator _notificator;

        public BaseController(INotificator notificator)
        {
            _notificator = notificator;
        }

        protected ActionResult CustomResponse(object result = null)
        {
            if (IsValidOperation())            
                return Ok(new { Success = true, data = result });

            return BadRequest(new
            {
                Success = false,
                Errors = string.Join(", ", _notificator.GetMessages().Select(s=> s.Message))
            });
        }

        protected ActionResult CustomResponse(ModelStateDictionary modelState)
        {
            var erros = modelState.Values.SelectMany(e => e.Errors);

            foreach (var erro in erros)
            {
                AddError(erro.ErrorMessage);
            }

            return CustomResponse();
        }

        protected bool IsValidOperation()
        {
            return !_notificator.HasNotification();
        }

        protected void AddError(string error)
        {
            _notificator.Handle(new Notification(error));
        }
    }
}
