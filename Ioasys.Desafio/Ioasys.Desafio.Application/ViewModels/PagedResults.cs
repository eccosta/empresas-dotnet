﻿using System.Collections.Generic;

namespace Ioasys.Desafio.Application.ViewModels
{
    public class PagedResults<T> where T : class
    {
        public IEnumerable<T> Results { get; set; }

        public int TotalResults { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string Query { get; set; }
    }
}
