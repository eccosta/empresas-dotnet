﻿using Ioasys.Desafio.Application.Extensions;
using Ioasys.Desafio.Application.Interfaces;
using Ioasys.Desafio.Application.Notifications;
using Ioasys.Desafio.Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Ioasys.Desafio.API.Controllers
{
    [Route("api/accounts")]
    [Authorize]
    public class IdentityController : BaseController
    {
        private readonly IAccountService _AccountService;

        public IdentityController(IAccountService AccountService, INotificator notificator) : base(notificator)
        {
            _AccountService = AccountService;
        }

        /// <summary>
        /// Get a list of all non administrator users.
        /// </summary>
        /// <param name="ps">Page Size</param>
        /// <param name="page">Page Index</param>
        /// <param name="q">Username to search</param>
        /// <returns></returns>
        [HttpGet("non-admin-accounts")]
        [ClaimsAuthorize("CustomRoles", "Admin")]
        public async Task<IActionResult> GetActiveNonAdminUsers([FromQuery] int ps = 8, [FromQuery] int page = 1, [FromQuery] string q = null)
        {
            return CustomResponse(await _AccountService.GetActiveNonAdmininUsers(page, ps, q));
        }

        [AllowAnonymous]
        [HttpPost("")]
        public async Task<IActionResult> Register(UserRegister userRegister)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            return CustomResponse(await _AccountService.RegisterAsync(userRegister));
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserLogin userLogin)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            return CustomResponse(await _AccountService.LoginAsync(userLogin));
        }

        [HttpPut]
        [AllowAnonymous]
        public async Task<IActionResult> UpdateUserInfo(UpdateUserViewModel userViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            return CustomResponse(await _AccountService.UpdateUserAsync(userViewModel));
        }


        [HttpPatch("{userId}/set-admin-role")]
        [ClaimsAuthorize("CustomRoles", "Admin")]
        public async Task<IActionResult> SetAdminRole(Guid userId)
        {
            return CustomResponse(await _AccountService.SetAdminRoleToUserAsync(userId));
        }

        [HttpPatch("{userId}/disable")]
        [ClaimsAuthorize("CustomRoles", "Admin")]
        public async Task<IActionResult> Disable(Guid userId)
        {            
            return CustomResponse(await _AccountService.DisableUserAsync(userId));
        }
    }
}
