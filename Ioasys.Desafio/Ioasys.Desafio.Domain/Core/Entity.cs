﻿using System;

namespace Ioasys.Desafio.Domain.Core
{
    public abstract class Entity
    {
        public Guid Id { get; set; }

        protected Entity()
        {
            Id = Guid.NewGuid();
        }
    }
}
