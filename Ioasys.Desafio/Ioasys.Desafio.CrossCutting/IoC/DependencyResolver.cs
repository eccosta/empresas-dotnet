﻿using Ioasys.Desafio.Application.Interfaces;
using Ioasys.Desafio.Application.Notifications;
using Ioasys.Desafio.Application.Services;
using Ioasys.Desafio.Data;
using Ioasys.Desafio.Data.Context;
using Ioasys.Desafio.Data.Repositories;
using Ioasys.Desafio.Domain.Identidade;
using Ioasys.Desafio.Domain.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ioasys.Desafio.CrossCutting
{
    public static class DependencyResolver
    {
        public static void ResolveServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<IdentityContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            }, ServiceLifetime.Transient);

            services.AddDbContext<MoviesContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));

            }, ServiceLifetime.Transient);

            services.AddDefaultIdentity<User>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<IdentityContext>()
                .AddDefaultTokenProviders();

            services.AddScoped<INotificator, Notificator>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IMovieService, MovieService>();
        }
    }
}
