﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ioasys.Desafio.Application.ViewModels
{
    public class MovieViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Title { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 10)]
        public string Description { get; set; }

        public List<DirectorViewModel> Directors { get; set; }

        public List<WriterViewModel> Writers { get; set; }

        public List<GenreViewModel> Genres { get; set; }

        public List<CasterViewModel> Casters { get; set; }

        public int Rating { get; set; }
    }

    public class DirectorViewModel
    {
        public string Name { get; set; }
    }

    public class WriterViewModel
    {        
        public string Name { get; set; }
    }

    public class GenreViewModel
    {        
        public string Name { get; set; }
    }

    public class CasterViewModel
    {
        public string Name { get; set; }
    }
}
